from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password, check_password
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.core.mail import send_mail
from myproject.settings import STATIC_ROOT, DOMAIN, ADMIN_EMAIL

from myproject.auth.models import User, Product, Bid, Comment, UrlCode, CC
import RSA
import random, datetime, string,re,math, copy

SQL_INJ = False

def index(request, logging_in=True, reg_err_msg='', reg_suc_msg='', fp_err_msg='', fp_suc_msg=''):
	err_msg = ''
	# create some dummy data if none exist
	if not User.objects.all():
		user = User.objects.create_user('alexan.nader@gmail.com', 'alexan.nader@gmail.com', 'pass')
		user.first_name = 'Nader'
		user.last_name = 'Alexan'
		user.save()
	if not Product.objects.all():
		Product(img='http://thumbs.ebaystatic.com/d/w225/m/mQRWCqZIo-2USZXISgEuipA.jpg', name='Apple iPad mini 16GB, Wi-Fi, 7.9in - White & Silver (Latest Model)', desc='Condition: New\nBrand: Apple\nStorage Capacity: 16 GB').save()
		Product(img='http://i.ebayimg.com/00/s/NjAwWDYwMA==/z/ZgYAAMXQHDlRcG9D/$T2eC16F,!zcE9s4g0tw9BRcG9DV1wQ~~60_35.JPG', name='Sony Cybershot DSC-H90 16.1 MP 16x Optical Zoom HD Video Superzoom Camera Black', desc='Condition:\nNew: A brand-new, unused, unopened, undamaged item in its original packaging (where packaging is applicable). Packaging should be the same as what is found in a retail store, unless the item is handmade or was packaged by the manufacturer in non-retail packaging, such as an unprinted box or plastic bag. See the seller\'s listing for full details').save()
		
	if request.user.is_anonymous():
		if request.POST and logging_in:
			email = request.POST['email']
			password = request.POST['password']
			if SQL_INJ:
				password = make_password(password, salt=email, hasher='md5')
				query = "SELECT * FROM auth_user WHERE username=\'" + email + "\'"#" AND password=\'" + password + "\'" 
				print '*', User.objects.raw(query)
				user = [p for p in User.objects.raw(query)][0]
			else:
				user = authenticate(username=email, password=password)
			if user is not None:
				if user.is_active:
					login(request, user)
				else:
					err_msg = 'User has been inactive for too long'
			else:
				err_msg = 'Email/Password combination is incorrect'
	return render_to_response('index.html', {'err_msg':err_msg, 'user':request.user, 
		'reg_err_msg':reg_err_msg, 'reg_suc_msg':reg_suc_msg,
		'fp_err_msg':fp_err_msg, 'fp_suc_msg':fp_suc_msg,
		'products':None if request.user.is_anonymous() else Product.objects.all()}, 
		context_instance=RequestContext(request))

def product(request, product_id, comment=False):
	if request.user.is_anonymous(): return index(logging_in=False) 
	try: product = Product.objects.get(pk=product_id)
	except: return index(logging_in=False)
	
	suc_msg = err_msg = ''
	if request.POST and not comment:
		try: bid = int(float(request.POST['bid']) * 100)
		except: err_msg = 'Allowed charcters in bid are digits and period'
		else:
			bids = Bid.objects.filter(user=request.user, product=product)
			if bids: 
				bids[0].bid = bid
				bids[0].save()
			else:
				Bid(user=request.user, product=product, bid=bid).save()
			suc_msg = 'Bid added successfully, to override it, re-bid'
	return render_to_response('product.html', {'product':product, #'bids':Bid.objects.filter(product=product),
		'suc_msg':suc_msg, 'err_msg':err_msg,
		'comments':Comment.objects.filter(product=product),
		}, context_instance=RequestContext(request))


def Logout(request):
	logout(request)
	return index(request, logging_in=False)


def addComment(request, product_id):
	try: prod = Product.objects.get(pk=product_id)
	except: return index(request, logging_in=False)
	if request.POST:
		content=request.POST['comment']
		Comment(user=request.user, content=content, product=prod).save()
	return product(request, product_id, comment=True)


def register(request):
	suc_msg = err_msg = ''
	if request.POST:
		first_name = request.POST['first_name']
		if not first_name: err_msg += "First name is required<br/>"
		
		last_name = request.POST['last_name']	
		if not last_name: err_msg += "Last name is required<br/>"	

		email = request.POST['email']
		if not email: err_msg += 'Email is required<br/>'
		else:
			if SQL_INJ:
				query = User.objects.raw("SELECT * FROM auth_user WHERE email='%s'" % email)
				user = [a for a in query][0]
				if user:
					err_msg += "This email is already registered<br/>"
			else:
				if User.objects.filter(email=email): err_msg += "This email is already registered<br/>"
		
		password = request.POST['password']
		if not password: err_msg += "Password is required<br/>"
		else: password = make_password(password, salt=email, hasher='md5')

		if not err_msg:
			user = User(email=email,username=email, password=password, first_name=first_name, last_name=last_name)
			user.save()
			suc_msg = 'You have registered successly, you may now login'
	return index(request, logging_in=False, reg_err_msg=err_msg, reg_suc_msg=suc_msg)

def changePass(request):
	user = request.user
	if user.is_anonymous(): return index(request)
	err_msg = suc_msg = ''
	if request.POST:
		old_pass = request.POST['old_pass']
		new_pass_1 = request.POST['new_pass_1']
		new_pass_2 = request.POST['new_pass_2']
		if not check_password(old_pass, user.password):
			err_msg = 'Old password is incorrect'
		elif new_pass_1 != new_pass_2:
			err_msg = 'New passwords don\'t match'
		else:
			user.set_password(new_pass_1)
			suc_msg = 'Password changed successfully'
	return render_to_response('changePass.html', {'err_msg':err_msg, 'suc_msg':suc_msg}, context_instance=RequestContext(request))

def forgotPass(request):
	err_msg = suc_msg = ''
	if request.POST:
		email = request.POST['email']
		if email:
			try: user = User.objects.get(username=email)
			except: err_msg = email + ' is not registered'
			else:	
				code = generateCode(10)
				urlCode = UrlCode(code=code, date=datetime.datetime.now(), user=user)
				urlCode.save()
				msg = 'To reset your password, go to: https://' + DOMAIN + '/' + urlCode.code + '\n\nIf you haven\'t requested an password reset, you can ignore this email, safetly and no changes will be made\n\nCheers.'
				send_mail('Help with your password', msg, ADMIN_EMAIL, [email], fail_silently=False)
				try: send_mail('Help with your password', msg, ADMIN_EMAIL, [email], fail_silently=False)
				except: err_msg = 'Error in sending emails, please check your internet connection'
				else: suc_msg = 'Instructions for reseting your password have been sent to your email'
		else:
			err_msg = 'Email cannot be left empty'
	return index(request, logging_in=False, fp_err_msg=err_msg, fp_suc_msg=suc_msg)

def generateCode(N):
    code = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for x in range(N))
    # make sure that the code is unique
    while(UrlCode.objects.filter(code=code)):
        code = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for x in range(N))
    return code

def resetPass(request, code):
    try: url_code = UrlCode.objects.get(code=code)
    except: 
        return HttpResponse('This url is incorrect.')
    if url_code.date + datetime.timedelta(0,5,0) < datetime.datetime.now():
    	url_code.delete()
    	return HttpResponse('Your session has expired, re-request a password reset')
    user = url_code.user
    new_pass = generateCode(5)
    subject = 'Your new password'
    msg = 'Your new password is: ' + new_pass
    user.set_password(new_pass)
    user.save()
    user.email_user(subject, msg, from_email=ADMIN_EMAIL)
    url_code.delete()
    return HttpResponse('Your password has been reset successfully and sent to your email')

def editCard(request):
	err_msg = suc_msg = ''
	if request.POST:
		ccNum = request.POST['ccNum']
		expDate = request.POST['expDate']

		try: ccNum = int(ccNum)
		except: err_msg += 'Credit Card number must be numeric<br/>'

		# if not re.match(r'\d\d[.]\d\d', expDate):
		
		try: cc = CC.objects.get(user=request.user)
		except: 
			cc = CC(user=request.user)
			cc.save()
		#base
		n = 3070966500664329385064608568944672763852650505939937960446311614709341896193799981806145664763856125668368451422335524686933171817201460728811512948472758893743461502117744844388568238349587208149296379
		#public key
		e = 791552300292173225900831763220458295535017309438374571399941558722353237550162643070719075305008003109096999852801213168829393950690761733801812850223059512815593322452396157279160532298549685508697763

		cipher = RSA.encrypt(str(ccNum), n, e, 8)
		cc.num = cipher
		cc.expDate = expDate
		cc.save()
	return render_to_response('cc.html', context_instance=RequestContext(request))

def card(request):
	err_msg = suc_msg = ''
	if request.POST:
		de = request.POST['de']
		#base
		n = 3070966500664329385064608568944672763852650505939937960446311614709341896193799981806145664763856125668368451422335524686933171817201460728811512948472758893743461502117744844388568238349587208149296379
		
		cc.num = RSA.decrypt(str(cc.num), n, de, 8)
	return render_to_response('ccview.html', context_instance=RequestContext(request))