from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Product(models.Model):
	img = models.TextField()
	name = models.CharField(max_length=100)
	desc = models.TextField()

	def __unicode__(self):
		return self.name

class Bid(models.Model):
	user = models.ForeignKey(User)
	product = models.ForeignKey(Product)
	bid = models.IntegerField()

	def __unicode__(self):
		return self.user

class Comment(models.Model):
	user = models.ForeignKey(User)
	product = models.ForeignKey(Product)
	content = models.TextField()

	def __unicode__(self):
		return str(self.user)

class CC(models.Model):
	user = models.ForeignKey(User)
	num = models.TextField()
	expDate = models.CharField(max_length=5)

	def __unicode__(self):
		return str(self.user)

class UrlCode(models.Model):
	user = models.ForeignKey(User)
	
	date = models.DateTimeField()
	code = models.CharField(max_length=10)

