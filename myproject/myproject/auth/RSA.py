# October 2011
# Author Yuhan Hao
# Email: yuhanhao@seas.upenn.edu
# Tested under python3.2.2

import random
import math
import copy

def euclid(a,b):
    '''returns the Greatest Common Divisor of a and b'''
    a = abs(a)
    b = abs(b)
    # make sure the algorithms still works even when
    # some negative numbers are passed to the program

    if a < b:
        a, b = b, a

    while b != 0:
        a, b = b, a % b

    return a

def coprime(L):
    '''returns 'True' if the values in the list L are all co-prime
       otherwier, it returns 'False'. '''
 
    for i in range (0, len(L)):
        for j in range (i + 1, len(L)):
            if euclid(L[i], L[j]) != 1:
                return False
 
    return True
 
def extendedEuclid(a,b):
    '''return a tuple of three values: x, y and z, such that x is
       the GCD of a and b, and x = y * a + z * b'''
    footprint = []
 
    # the boolean flag is used to make sure this function can return
    # right answer no matter which is bigger.
    if a < b:
        isASmallerThanB = True
        a, b = b, a
    else:
        isASmallerThanB = False
    while b != 0:
        footprint.append((a % b, 1, a, -(a//b), b))
        # for each tuple in list footprint
        # footprint[i][0] == footprint [i][1] * footprint[i][2]
        #                  + footprint [i][3] * foorprint[i][4]
        # and
        # footprint[i][4] == footprint[i+1][0]
        # this two equations are key to generate the linear combination
        # of a and b so that the result will be their GCD (or GCF).
        a, b = b, a % b
 
    # Start work backward to compute the linear combination
    # of a and b so that this combination gives the GCD of a and b
    footprint.reverse()
    footprint.pop(0)
    #print (footprint)
    x = footprint[0][1]
    y = footprint[0][3]
    #print (x, y)
    for i in range (1, len(footprint)):
        x_temp = x
        y_temp = y
        x = y_temp * footprint[i][1]
        y = y_temp * footprint[i][3] + x_temp
        #print (x, y)
 
    if (isASmallerThanB != True):
        return (a, x, y)
    else:
        return (a, y, x)
 
def modInv(a,m):
    '''returns the multiplicative inverse of a in modulo m as a
       positve value between zero and m-1'''
    # notice that a and m need to co-prime to each other.
    if coprime([a, m]) == False:
        return 0
    else:
        linearcombination = extendedEuclid(a, m)
        return linearcombination[1] % m
 
def crt(L):
    '''takes in a list of two or more tuples, ie
              L = [(a0, n0), (a1,n1),(a2,n2)...(ak nk)]
       if the n-s are not co-prime, this function prints an error message to
       the screen and returns -1. Otherwise it continues with the Chinese
       Remainder theorem, finding a value for x to return which satisfies
                    x = ai(  mod ni)
       for all tuples in the list L. This value must be between 0 and N-1
       where N is the product of all the n in the list L'''
    NList = []
    for item in L:
        NList.append(item[1])
    if coprime(NList) == False:
        print ("The input is not valid!")
        return -1
    else:
        bigN = 1
        for numbers in NList:
            bigN *= numbers
    CRTresult = 0
    for item in L:
        ai = item[0]
        Ci = bigN//item[1]
        #print (Ni, ai)
        Yi = extendedEuclid(Ci, item[1])
        #print (Ci, item[1])
        CRTresult += ai * Ci * Yi[1]
    return CRTresult % bigN
 

def extractTwos(m):
    '''m is a positive integer. A tuple (s, d) of integers is returned
    such that m = (2 ** s) * d.'''
    # the problem can be break down to count how many '0's are there in
    # the end of bin(m). This can be done this way: m & a stretch of '1's
    # which can be represent as (2 ** n) - 1.
    assert m >= 0
    i = 0
    while m & (2 ** i) == 0:
        i += 1
    return (i, m >> i)
 
 
def int2baseTwo(x):
    '''x is a positive integer. Convert it to base two as a list of integers
    in reverse order as a list.'''
    # repeating x >>= 1 and x & 1 will do the trick
    assert x >= 0
    bitInverse = []
    while x != 0:
        bitInverse.append(x & 1)
        x >>= 1
    return bitInverse
 
def modExp(a,d,n):
    '''returns a ** d (mod n)'''
    # a faster algorithms discussed in CIT 592 class
    assert d >= 0
    assert n >= 0
    base2D = int2baseTwo(d)
    base2DLength = len(base2D)
    modArray = []
    result = 1
    for i in range (1, base2DLength + 1):
        if i == 1:
            modArray.append(a % n)
        else:
            modArray.append((modArray[i - 2] ** 2) % n)
    for i in range (0, base2DLength):
        if base2D[i] == 1:
            result *= base2D[i] * modArray[i]
    return result % n

#------------------------------------------------------------

def string2numList(strn):
    '''Converts a string to a list of integers based on ASCII values'''
    # Note that ASCII printable characters range is 0x20 - 0x7E
    returnList = []
    for chars in strn:
        returnList.append(ord(chars))
    return returnList
 
def numList2string(L):
    '''Converts a list of integers to a string based on ASCII values'''
    # Note that ASCII printable characters range is 0x20 - 0x7E
    returnList = []
    returnString = ''
    for nums in L:
        returnString += chr(nums)
    return returnString
 
def numList2blocks(L,n):
    '''Take a list of integers(each between 0 and 127), and combines them into block size
    n using base 256. If len(L) % n != 0, use some random junk to fill L to make it '''
    # Note that ASCII printable characters range is 0x20 - 0x7E
    returnList = []
    toProcess = copy.copy(L)
    if len(toProcess) % n != 0:
        for i in range (0, n - len(toProcess) % n):
            toProcess.append(random.randint(32, 126))
    for i in range(0, len(toProcess), n):
        block = 0
        for j in range(0, n):
            block += toProcess[i + j] << (8 * (n - j - 1))
        returnList.append(block)
    return returnList
 
def blocks2numList(blocks,n):
    '''inverse function of numList2blocks.'''
    toProcess = copy.copy(blocks)
    returnList = []
    for numBlock in toProcess:
        inner = []
        for i in range(0, n):
            inner.append(numBlock % 256)
            numBlock >>= 8
        inner.reverse()
        returnList.extend(inner)
    return returnList
 
def encrypt(message, modN, e, blockSize):
    '''given a string message, public keys and blockSize, encrypt using
    RSA algorithms.'''
    cipher = []
    numList = string2numList(message)
    numBlocks = numList2blocks(numList, blockSize)
    for blocks in numBlocks:
        cipher.append(modExp(blocks, e, modN))
    return cipher
 
def decrypt(secret, modN, d, blockSize):
    '''reverse function of encrypt'''
    numBlocks = []
    numList = []
    for blocks in secret:
        numBlocks.append(modExp(blocks, d, modN))
    numList = blocks2numList(numBlocks, blockSize)
    message = numList2string(numList)
    return message


#----------------- test-------------------------

'''excpect output similar to
5594003822947220
[2462431126277060891683030720644929221955724696716525857718311827126208198260224337797678780L]
5594003822947220
'''

#base
n = 3070966500664329385064608568944672763852650505939937960446311614709341896193799981806145664763856125668368451422335524686933171817201460728811512948472758893743461502117744844388568238349587208149296379
#public key
e = 791552300292173225900831763220458295535017309438374571399941558722353237550162643070719075305008003109096999852801213168829393950690761733801812850223059512815593322452396157279160532298549685508697763
#private key
d = 965960145923493228657767341244543978930939347639505831487088415943065916007885977956971744719393862188647898317461696345109612198897492824272555793479714074357246638215441177489953673505458178999326027

message = '''5594003822947220'''
print(message)
cipher = encrypt(message, n, e, 8)
print(cipher)
Amessage = decrypt(cipher, n, d, 8)
print(Amessage)
