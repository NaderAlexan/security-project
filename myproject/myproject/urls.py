from django.conf.urls import patterns, include, url
import settings
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
   url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
   url(r'^$', 'myproject.auth.views.index'),
   url(r'^/$', 'myproject.auth.views.index'),
   url(r'^addComment/(?P<product_id>\d+)/$', 'myproject.auth.views.addComment', name='addComment'),
   url(r'^product/(?P<product_id>\d+)/$', 'myproject.auth.views.product', name='product'),
   url(r'^bid/(?P<product_id>\d+)/$', 'myproject.auth.views.product', name='bid'),
   url(r'^register/$', 'myproject.auth.views.register', name='register'),
   url(r'^editCard/$', 'myproject.auth.views.editCard', name='editCard'),
   url(r'^card/$', 'myproject.auth.views.card', name='card'),
   url(r'^changePass/$', 'myproject.auth.views.changePass', name='changePass'),
   url(r'^forgotPass/$', 'myproject.auth.views.forgotPass', name='forgotPass'),
   url(r'^logout/$', 'myproject.auth.views.Logout', name='logout'),

    # This url must always be at the end because it will match with anything
    url(r'^(?P<code>\w+)/$','myproject.auth.views.resetPass'),
    
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
